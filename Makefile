# Copyright (c) 2000 Martin Hinner <mhi@penguin.cz>
# The original Makefile was removed.

WP2X_DIR = /usr/local/lib/wp2x
BINDIR = /usr/local/bin
MANDIR = /usr/local/man
VERSION = 2.5

all:	wp2x

wp2x:
	cd src && make wp2x WP2X_DIR=$(WP2X_DIR) VERSION=$(VERSION)

install:
	mkdir -p $(BINDIR)
	mkdir -p $(MANDIR)/man1
	mkdir -p $(WP2X_DIR)
	cd src && make install BINDIR=$(BINDIR) WP2X_DIR=$(WP2X_DIR)
	cd cfg && make install BINDIR=$(BINDIR) WP2X_DIR=$(WP2X_DIR)
	cd doc && make install MANDIR=$(MANDIR) WP2X_DIR=$(WP2X_DIR)

depend: 
	cd src && make depend

clean:
	rm -f *~ *.o core
	cd src && make clean
	cd cfg && make clean

test:	wp2x
	cd test && make test

gentest:wp2x
	cd test && make gentest
