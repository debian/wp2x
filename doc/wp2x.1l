.TH WP2X 1
.SH NAME
 wp2x \- A WordPerfect 5.0 to whatever converter
.SH SYNOPSIS
.B wp2x
[
.B \-s
] [
.B \-v
] [
.BI \-n blip
] configfile wpfile
.SH DESCRIPTION
.de Tx
.if n TeX\\$1
.if t T\v'12u'E\v'-12u'X\\$1
..
.de Lt
.if n LaTeX\\$1
.if t L\v'-12u'A\v'12u'T\v'12u'E\v'-12u'X\\$1
..
.I Wp2x
is intended to convert
.I simple
files stored in WordPerfect 5.1 format into any other document processing
language that uses plain text files.  Examples include
.Tx ,
.Lt ,
troff, GML and HTML.
.PP
.I Wp2x
reads a configuration file and a WordPerfect 5.1 input file, and
uses the information in them to produce an output file, which
is sent to stdout.
If the configuration file cannot be found, a suffix of
.I .cfg
is appended.
The current directory is searched, as well as the lib directory
specified by the 
.I WP2X_DIR
variable in the Makefile. (Usually
.I /usr/local/lib/wp2x
) and the directories specified by the environment variables
.I PATH ,
.I DPATH ,
and
.I WP2XLIB .
.PP
.fi
Some codes are not translated because documents that require these
codes typically would require significant hand-editing.
Hence, there's no point in trying to emulate something you're going
to delete anyway.
(Remember,
.I wp2x
is not intended to be used as an automated conversion program.
Rather, it is intended to be used as a single step in the document
conversion process, which gets most of the the grunt work of conversion
done and out of the way, so that you can concentrate your efforts on
converting the trickier parts of the document.  The object of the
game is to produce a readable conversion, rather than a perfect conversion.)
.PP
As the program runs, a dot is printed to
.I stderr
for every 1024 characters converted.  This can be suppressed with the
.B \-s
switch, and the interval between dots can be changed with the
.B -n
switch.
.SH OPTIONS
.TP
.B \-s
Suppresses all non-error output to
.IR stderr ,
including the
.I typeout
banner, the progress dots, and warnings about undefined expansions.
.TP
.BI \-n blip
Every
.I blip
tokens, a dot is emitted to
.IR stderr ,
unless the
.B \-s
switch is given.
The value
.I blip
must appear imediately following the
.B \-n
without an intervening space.
If no
.B \-n
switch is supplied, then a value of 1024 is assumed.
.TP
.B \-v
prints the version number and the program usage.
.SH USAGE
The configuration file controls how the file is converted from WordPerfect 5.1
format.  Each line of the configuration file is of the form
.sp 1
.ce 1
identifier="list of codes"
.sp 1
where the list of codes is a string which will be placed in the
output stream whenever the corresponding WordPerfect code is
encountered.
Standard C-style backslash-escape sequences are recognized, as well
as \exFF for hex values.
You do not have to backslash-protect a newline.  Some identifiers
supply replacable parameters, which can be interpolated as follows:
.TP
%1
interpolate first parameter as a decimal integer.
.TP
%2
interpolate second parameter as a decimal integer.
.TP
%c
interpolate first parameter as an ASCII character.
.TP
%\en
interpolate a newline if the most-recently-output character was not
already a newline.
(The
.I \en
can be either the C-style escape sequence, or an actual newline character.)
Use this if the expansion must take place at the beginning of a line.
(For example,
.I troff
control characters must appear as the first character in the line in
order to take effect.)
This sequence is meaningful only at the beginning of the string;
if it appears elsewhere, it is flagged as erroneous.
.TP
%%
interpolate a percent-sign.
.PP
A percent sign followed by any other character is considered an error.
It is also an error to interpolate a parameter that is not applicable
to the identifier being defined.  You may interpolate the parameters
as many times as, and in whatever order, you wish.  (With the
exception of the
.I %\en
code.)
.PP
Here follows a list of the accepted identifiers.
In the discussion, `%1' represents the first parameter, and `%2'
the second.  Remember that the character version of %1 is available
as `%c'.
.sp 1
.PD 0
.TP 15
BEGIN
Expanded at the beginning of the file.
.TP
END
Expanded at the end of the file.
.TP
COMMENT
Expanded when wp2x needs to insert a comment into the output.
The comment is passed as %s.
.sp 1
.TP
PageNo
Insert current page number
.TP
RomanPage
Set page number to %1, and set roman-numeral mode
.TP
ArabicPage
Set page number to %1, and set arabic-numeral mode
.sp 1
.TP
Tab
What to do when you see a tab character.
.TP
BeginTabs
Emitted when tab settings are about to change.
The
.B BeginTabs
code should delete all existing tabs
and prepare for new tab settings to start.
All tab values are given in columns measured
from the physical left edge of the paper.
(Not from the left margin.)
.TP
SetTab
Set a normal (left-justified) tabstop at column %1.
.TP
SetTabCenter
Set a centered tabstop at column %1.
.TP
SetTabRight
Set a right-justified tabstop at column %1.
.TP
SetTabDecimal
Set a decimal tab at column %1.
.TP
EndTabs
Finish the setting of tabstops.
.sp 1
.PP
For example, if the WordPerfect file contains a code that says
`Set new tabstops as follows:  Regular tab at column 15,
a centered tab at column 40, a right-justified tab at column 59,
and a regular tab at column 60', then the following expansions are
made in succession:
.nf

    BeginTabs
    SetTab(15)
    SetTabCenter(40)
    SetTabright(59)
    SetTab(60)
    EndTabs

.fi
.TP
HSpace
Hard (nonbreakable) space.
.TP
HPg
Hard page break.
.TP
CondEOP
Force a new page if fewer than %1 half-lines remain on current page.
.sp 1
.TP
HRt
Hard return.
.TP
SRt
Soft return.
.sp 1
.TP
\-
Breakable hyphen.
.TP
\-\-
Breakable hyphen, appearing at the end of a line.
.TP
=
Non-breakable hyphen.
.TP
\e\-
Discretionary hyphen.
.TP
\e\-\-
Discretionary hyphen, appearing at the end of a line.
.sp 1
.TP
Marg
Set left margin at %1 characters and right margin at %2 characters.
.TP
TopMargin
Set top margin to %1 lines.
.TP
PageLength
Set page length to %1 lines.
.sp 1
.TP
SS
Single spacing.
.TP
DS
Double spacing.
.TP
1.5S
One-and-a-half spacing.
.TP
TS
Triple spacing.
.TP
LS
Other line spacing.  %1 is twice the desired spacing.
(For example, a request for 2.5-spacing sets %1=5.)
.TP
LPI
Set %1 lines per inch (%1 is either 6 or 8)
.sp 1
.TP
Bold
Begin boldface
.TP
bold
End boldface
.TP
Und
Begin underline
.TP
und
End underline
.TP
DoubleUnd
Begin double underline
.TP
doubleund
End double underline
.TP
Red
Begin redline
.TP
red
End redline
.TP
Strike
Begin strikeout
.TP
strike
End strikeout
.TP
Rev
Begin reverse video
.TP
rev
End reverse video
.TP
Outline
Begin outline text
.TP
outline
End outline text
.TP
Fine
Begin fine font size
.TP
fine
End fine font size
.TP
Over
Begin overstrike font
.TP
over
End overstrike font
.TP
Sup
Begin superscript
.TP
sup
End superscript
.TP
Sub
Begin subscript
.TP
sub
End subscript
.TP
Large
Begin large font size
.TP
large
End large font size
.TP
Small
Begin small font size
.TP
small
End small font size
.TP
VeryLarge
Begin very large font size
.TP
verylarge
End very large font size
.TP
ExtraLarge
Begin extra large font size
.TP
extralarge
End extra large font size
.TP
Italics
Begin an italics font
.TP
italics
End an italics font
.TP
Shadow
Begin shadow font
.TP
shadow
End shadow font
.TP
SmallCaps
Begin small capitals font (fixed width)
.TP
smallcaps
End small capitals font (fixed width)
.sp 1
.TP
UpHalfLine
Advance printer up 1/2 line
.TP
DownHalfLine
Advance printer down 1/2 line
.TP
AdvanceToHalfLine
Advance to absolute vertical position.
%1 is what WordPerfect thinks the current vertical page position is,
in half-lines.  %2 is the desired position, also in half-lines.
.sp 1
.TP
Indent
Expanded when an "Indent" code appears.
.TP
indent
Expanded at the end of an indented paragraph.
.TP
DIndent
Expanded when a "left-and-right-indent" code appears.
.TP 
dindent
Expanded at the end of an double indent
.TP
MarginRelease
Margin release.  %1 is the number of characters to move left.
.sp 1
.TP
Center
Center current line
.TP
center
End centering
.TP
CenterHere
Center line around current column
.TP
centerhere
End centering
.sp 1
.TP
Align
Begin alignment
.TP
align
End alignment
.TP
AlignChar
Set alignment character
.TP
FlushRight
Begin flush right
.TP
flushright
End flush right
.sp 1
.TP
Math
Begin math mode
.TP
math
End math mode
.TP
MathCalc
Begin math calc mode
.TP
MathCalcColumn
Math calc column
.sp 1
.TP
SubTotal
Do subtotal
.TP
IsSubTotal
Subtotal entry
.TP
Total
Do total
.TP
IsTotal
Total entry
.TP
GrandTotal
Do grand total
.sp 1
.TP
Col
Begin column mode
.TP
col
End column mode
.sp 1
.TP
Fn
Expanded at the beginning of a footnote.
.TP
fn
Expanded at the end of a footnote.
.TP
En
Expanded at the beginning of an endnote.
.TP
en
Expanded at the end of an endnote.
.TP
SetFn#
Set the number for the next footnote to %1.
.TP
FNote#
Footnote number.
.TP
ENote#
Endnote number.
.TP
Figure#
Figure number.
.TP
TableMarker
Insert table of contents here
.sp 1
.TP
Hyph
Enable hyphenation.
.TP
hyph
Disable hyphenation.
.TP
Just
Enable justification.
.TP
just
Disable justification.
.TP
Wid
Enable widow/orphan protection.
.TP
wid
Disable widow/orphan protection.
.TP
HZone
The hyphenation zone.  %1 and %2 are the two magical values
that WordPerfect uses to control hyphenation.
.TP
DAlign
Set the decimal alignment character to that whose ASCII value is %1.
(`%c' is useful here.)
.sp 1
.TP
Header
Begin header text
.TP
header
End header text
.TP
Footer
Begin footer text
.TP
footer
End footer text
.sp 1
.TP
Supp
Suppress page number/header/footer information for one page.
%1 argument is a bit field which describes what sort of
suppression is desired.  Here's what the bits mean:
.nf
          1 = all
          2 = page number
          4 = page numbers moved to bottom
          8 = all headers
         16 = header a
         32 = header b
         64 = footer a
        128 = footer b
.fi
.TP
CtrPg
Center page vertically
.sp 1
.TP
SetFont
Change pitch or font.  %1 is the desired pitch.  (Negative
means proportionally-spaced.)  %2 is the font number.
.TP
SetBin
Select paper bin to %1 = 0, 1, ...
.sp 1
.TP
PN0
No page numbering.
.TP
PN1
Page number in top left.
.TP
PN2
Page number in top center.
.TP
PN3
Page number in top right.
.TP
PN4
Page number on top outside corners (even/odd).
.TP
PN5
Page number in lower left.
.TP
PN6
Page number in bottom center.
.TP
PN7
Page number in lower right.
.TP
PN8
Page number on bottom outside corners (even/odd).
.PD
.PP
If no expansion is supplied for an identifier, then nothing is emitted to
.IR stdout ,
but a warning message is sent to
.IR stderr .
This warning message will appear at most once per identifier, and it
can be suppressed completely by the
.B \-s
option.
.PP
The special identifier
.I typeout
causes its replacement text to be
displayed on the screen every time the configuration file is read.
This is useful for identification messages, or reminders to the user.
.PP
A special identifier is any character enclosed in single quotation
marks, which represent themselves.  For example,
.nf

   '\(*a'="{\e\ealpha}"

.fi
causes the string "{\ealpha}" to be emitted when an \(*a is encountered.
This could also have been written as
.nf

   '\exE0'="{\e\ealpha}"

.fi
if the character \(*a has ASCII value 0xE0.
(Which is true for the IBM PC encoding.)
.PP
If no definition exists for a particular special character,
it is transmitted undisturbed.  If a special character is encountered
from the upper half of the ASCII character set, and if it has no
definition, then a warning message is also emitted.
(Which can be suppressed with the
.B \-s
option.)
.PP
Lines beginning with the # character are comments.
.SH NOTES

This is based on an original WP 4.2 to anything translator. The file
format has changed a lot between 4.2 and 5.0. This translator no
longer reads WP 4.2 files, although it could be extended to do so. 

The 5.0+ format starts with a standard header file. There is a four
byte magic number at the head of the file, followed by various product
and version information. All WordPerfect Corporation utilities use
this standard header. See the WPproducts array in 
.I wp2x.c

Once the contents of the file have been located, there are three kinds
of codes: simple one byte controls (WP 4.2 had only these kinds),
fixed length controls, and variable length controls. There are a large
number of undefined types defined for future use. If 
.B wp2x
detects something it doesn't understand, it can extract the length and
skip that code. There are a number of defined codes that are
unimplemented. Please see the code, specifically 
.I tokens.c
where much of the input processing is done.

.SH FILES
The sample configuration files in
.I /usr/local/lib/wp2x
give you some sort of idea what a `production quality'
configuration file might look like.
They are not intended to be used as-is, but rather are meant to be
modified to suit your particular needs.
.SH "SEE ALSO"
.IR tex (1),
.IR latex (1),
.IR nroff (1),
.IR troff (1),
.IR WordPerfect Developer's Toolkit
.IR getopt(3).
.SH DIAGNOSTICS
.TP
Ignoring byte [XX]
Indicates that an unimplemented single byte code was ignored.
.TP
Ignoring fixed [XX]
Indicates that an unimplemented fixed length code was ignored.
.TP
Ignoring variable [XX] sub [XX] length
Indicates that an unimplemented variable length code was ignored, and
gives its length.
.TP
Warning: Expected XX but received XX at pos: YYYY
something is wrong in the input file at byte YYYY.
.TP
Warning: No expansion for XX (C)
A WP code for which no expansion was defined in the config file was
encountered.
.TP
Internal error: Invalid escape C
An error occured while processing an expansion escape (%x
substitution). Probably it was not a recognized escape, check the
config file.
.TP
Fixed Length block [XX] incorrectly terminated by [YY] as pos Z
Something is wrong with the input file, a fixed length block was
screwed up.
.TP
Reserved code [XX] seen
Something that WPC defined as reserved was seen. Check with WPC for
new meaning.
.TP
Not a recognized file type. The file did not start with the right WPC
magic number. Maybe this is a 4.2 file, or not a WordPerfect file at all?
.TP
Error: Cannot open X (reason)
The file X could not be opened, for the indicated reason.
.TP
Error: Expecting a hex digit
Inside a string, you typed the characters `\ex', but the next character
was not a valid hex digit.
.TP
Error: string pool overflow
The configuration file contained too many strings.  Increase the value
of POOL_SIZE and recompile.
.TP
Error: Unknown identifier X
The word X was encountered in the configuration file when
.I wp2x
expected a token identifier like `HRt'.
Most likely, you either misspelled it, or you got your quotation marks
out of sync.
.TP
Error: Identifier not followed by = sign
After an identifier must come an equals-sign.
.TP
Error: Quotation mark expected
After the equals-sign must come a quotation mark.
.TP
Error: X: `%\en' not at start of expansion
The expansion for the identifier X contained the indicated sequence
of characters somewhere other than the beginning of the string.
The `%\en' interpolation code is meaningful only at the beginning
of a string.
.TP
Error: X: invalid escape `%x'
The expansion for the identifier X contained an invalid escape.
Either you used `%1', `%2' or `%c' when the identifier X does not
supply that parameter, or you meant for a genuine percent sign
to be output, in which case you should put `%%' in the expansion.
.TP
Error: Invalid character identifier
Character identifiers can only be one character long (after backslash
interpretation).
.TP
Warning: Expected XX but received YY.
The program expected the next byte from the WP file to be XX, but
the byte YY was encountered instead.  This means either that your WP
file is damaged, or that the program is seriously confused.  (Or both.)
The program
will pretend that the byte in the file was indeed XX, which may
lead to synchronization errors later on.
.TP
Warning: No expansion for X
The WP file contained the token X, but the configuration file did not
contain any expansion text for it.  A null expansion was assumed.
.TP
Warning: No expansion for XX (c)
The WP file contained the character c (hex code XX),
but the configuration file
did not contain any expansion text for it.
The character was emitted unaltered.
Beware that this may give your text formatter indigestion if it does
not handle eight-bit characters.
.TP
Warning: X code not supported
The file being converted uses a code which
.I wp2x
does not know how to convert.
A comment is placed in the output file in its place.
If you ever encounter a `WPCorp reserved' or a `WPCorp undefined' code,
the author would appreciate hearing from you.
.TP
Internal error:  Invalid escape, %x
While processing text,
.I wp2x
noticed that you used an invalid escape.
Nothing is emitted as the escape text.
(The internal-ness is that this error is supposed to be caught at
the time the configuration file is read.)
.SH BUGS
Naive configuration files will fail if your WP file
doesn't nest its tags properly.  A typical case is
.nf

     [Center][B]Hello[center]
     [Center]There[b][center]
.fi
to produce a centered boldface `Hello'.  If you use the naive encoding
of
.nf

     Center="\e\ecenterline{"
     center="}\en"
     Bold="{\e\ebf "
     bold="}"

.fi
then this will expand to
.nf

     \ecenterline{{\ebf Hello}
     \ecenterline{There}}

.fi
WordPerfect has no clean concept of grouping; it lets you change fonts
at any time and let those changes propagate outside the current
environment.
(With the exception of headers, footers, footnotes, and endnotes.)
.PP
Now sure, you could write complicated configuration strings to try
to handle this `properly', but it'd probably not be worth the trouble.
After all, the purpose is not to perform a perfect conversion, but
rather to produce a
.I readable
conversion, which can then be massaged by hand to produce a perfect
manuscript.
.PP
Another potential problem is combined attributes, like boldface underline.
Under a naive configuration,
.nf

     [B]Boldface [U]Underlined boldface[b] Underlined[u] normal.

.fi
comes out as
.nf

 {\ebf Boldface {\eit Underlined boldface} Underlined\e/} normal.

.fi
which is wrong for two reasons.  One is the nesting problem discussed
above.  The other is that
.Tx
font attributes do not combine.
.PP
Similar problems exist for other document preparation systems.
So be careful.

.SH AUTHORS

Original author: Raymond Chen <raymond@math.berkeley.edu>
Previous maintainer: Michael Richardson <mcr@ccs.carleton.ca>
Current maintainer: Martin Hinner <mhi@penguin.cz>
