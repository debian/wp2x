#!/usr/local/bin/perl

# things this script does:
#    2. converts single lines followed by <p> to <PRE> blocks.

$sawp=0;
$sawnonp=0;

while(<>) {
    if($ARGV ne $oldargv) {
	rename($ARGV, $ARGV . '.2.bak');
	open(ARGVOUT, ">$ARGV");
	select(ARGVOUT);
	$oldargv = $ARGV;
    }

    chop;

    if(/^\<[pP]\>$/) {
	if($sawsingle==1) {
	    if(!$startpre) {
		$startpre=1;
		print "<PRE>\n";
	    }
	}
    }
    else {
	if(!/\</ && !/\>/) {
	    $sawsingle++;
	    if($sawsingle>1 && $startpre) {	# have seen a second line with no intervening <p>
		print "</PRE>\n";	
		$startpre=0;
	    }
	}

	print "$_\n";
    }

}


    
    
