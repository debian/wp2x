/*
 * Definitions for each WP token.
 * 
 *
 */

#include <stdio.h>

#include "tokens.h"
#include "charsets.h"

extern int silent;

int
popEnvironment (state * st, struct WPToken *wpt, void *data)
{
  extern void leave_environment (state *, int);
  leave_environment (st, wpt->wp_intdata);
  return 1;
}

int
hardReturn (state * st, struct WPToken *wpt, void *data)
{
  process0 (st, HRt);
  return 1;
}

int
softPage (state * st, struct WPToken *wpt, void *data)
{
  return 1;
}

int
hardReturnSoftPage (state * st, struct WPToken *wpt, void *data)
{
  hardReturn (st, wpt, data);
  softPage (st, wpt, data);
  return 1;
}

int
ignoreByte (state * st, struct WPToken *wpt, void *data)
{
  if (!silent)
    {
      fprintf (stderr, "Ignoring byte [%02x]\n", wpt->wp_code);
    }
  return 1;
}

int
ignoreFixed (state * st, struct WPToken *wpt, void *data)
{
  if (!silent)
    {
      fprintf (stderr, "Ignoring fixed [%02x]\n", wpt->wp_code);
    }
  return (1);
}

int
ignoreVar (state * st, struct WPToken *wpt, void *data)
{
  struct VariableCode *vc = data;
  if (!silent)
    {
      fprintf (stderr, "Ignoring variable [%02x] sub [%02x] length: %d\n",
	       vc->func, vc->subFunc, vc->len);
    }
  return 1;
}

int
hardPage (state * st, struct WPToken *wpt, void *data)
{
  process0 (st, HPg);
  return 1;
}

int
softReturn (state * st, struct WPToken *wpt, void *data)
{
  process0 (st, SRt);
  return 1;
}

int
outputToken (state * st, struct WPToken *wpt, void *data)
{
  process0 (st, wpt->wp_intdata);
  return 1;
}

int
extendedChar (state * st, struct WPToken *wpt, void *data)
{
  char *bytes = data;
  int char_code = bytes[1];
  int char_set = bytes[2];

  if (char_set >= 0 && char_set <= 8)
    fputs (isolatin1_chars[char_set][char_code], st->outfile);
  else
    fprintf (st->outfile, "<!-- CS=%d, CC=%d -->", char_set, char_code);

  return 1;
}

int
attributeOn (state * st, struct WPToken *wpt, void *data)
{
  char *bytes = data;

  char attribute = bytes[1];

  switch (attribute)
    {
    case 0:			/* extra large */
      process0 (st, ExtraLarge);
      break;
    case 1:			/* very large */
      process0 (st, VeryLarge);
      break;
    case 2:			/* large */
      process0 (st, Large);
      break;
    case 3:			/* small */
      process0 (st, Small);
      break;
    case 4:			/* fine */
      process0 (st, Fine);
      break;
    case 5:			/* superscript */
      process0 (st, Sup);
      break;
    case 6:			/* subscript */
      process0 (st, Sub);
      break;
    case 7:			/* outline */
      process0 (st, Outline);
      break;
    case 8:			/* italics */
      process0 (st, Italics);
      break;
    case 9:			/* shadow */
      process0 (st, Shadow);
      break;
    case 10:			/* redline */
      process0 (st, Red);
      break;
    case 11:			/* double underline */
      process0 (st, DoubleUnd);
      break;
    case 12:			/* bold */
      process0 (st, Bold);
      break;
    case 13:			/* strikeout */
      process0 (st, Strike);
      break;
    case 14:			/* underline */
      process0 (st, Und);
      break;
    case 15:			/* small caps */
      process0 (st, SmallCaps);
      break;
    }
  return 1;
}

int
attributeOff (state * st, struct WPToken *wpt, void *data)
{
  char *bytes = data;
  char attribute = bytes[1];

  switch (attribute)
    {
    case 0:			/* extra large */
      process0 (st, eExtraLarge);
      break;
    case 1:			/* very large */
      process0 (st, eVeryLarge);
      break;
    case 2:			/* large */
      process0 (st, eLarge);
      break;
    case 3:			/* small */
      process0 (st, eSmall);
      break;
    case 4:			/* fine */
      process0 (st, eFine);
      break;
    case 5:			/* superscript */
      process0 (st, eSup);
      break;
    case 6:			/* subscript */
      process0 (st, eSub);
      break;
    case 7:			/* outline */
      process0 (st, eOutline);
      break;
    case 8:			/* italics */
      process0 (st, eItalics);
      break;
    case 9:			/* shadow */
      process0 (st, eShadow);
      break;
    case 10:			/* redline */
      process0 (st, eRed);
      break;
    case 11:			/* double underline */
      process0 (st, eDoubleUnd);
      break;
    case 12:			/* bold */
      process0 (st, eBold);
      break;
    case 13:			/* strikeout */
      process0 (st, eStrike);
      break;
    case 14:			/* underline */
      process0 (st, eUnd);
      break;
    case 15:			/* small caps */
      process0 (st, eSmallCaps);
      break;
    }

  return 1;
}

int
endIndent (state * st, struct WPToken *wpt, void *data)
{
  /* do nothing */
  return 1;
}

int
startIndent (state * st, struct WPToken *wpt, void *data)
{
  char *bytes = data;

  if (bytes[1] & 1)
    {
      /* left/right indent */
      process0 (st, DIndent);
    }
  else
    {
      process0 (st, Indent);
    }
  do
    {
      if (process_token (st) == 0)
	return 0;
    }
  while (!(st->lastToken->wp_handleFunction == endIndent));

  if (bytes[1] & 1)
    {
      /* left/right indent */
      process0 (st, eDIndent);
    }
  else
    {
      process0 (st, eIndent);
    }
  return 1;
}



struct WPToken wp_tokens[] =
{
/* wp_type     wp_code  wp_intdata wp_strdata wp_size    wp_handleFunction */
  {reserved, 0x00, 0, NULL, 0, NULL},
  {singleByte, 0x01, 0, NULL, 0, ignoreByte},
  {singleByte, 0x02, 0, NULL, 0, ignoreByte},
  {singleByte, 0x03, 0, NULL, 0, ignoreByte},
  {singleByte, 0x04, 0, NULL, 0, ignoreByte},
  {singleByte, 0x05, 0, NULL, 0, ignoreByte},
  {singleByte, 0x06, 0, NULL, 0, ignoreByte},
  {singleByte, 0x07, 0, NULL, 0, ignoreByte},
  {reserved, 0x08, 0, NULL, 0, NULL},
  {reserved, 0x09, 0, NULL, 0, NULL},
  {singleByte, 0x0A, 0, NULL, 0, hardReturn},
  {singleByte, 0x0B, 0, NULL, 0, softPage},
  {singleByte, 0x0C, 0, NULL, 0, hardPage},
  {singleByte, 0x0D, 0, NULL, 0, softReturn},
  {singleByte, 0x0E, 0, NULL, 0, ignoreByte},
  {singleByte, 0x0F, 0, NULL, 0, ignoreByte},
  {singleByte, 0x10, 0, NULL, 0, ignoreByte},
  {singleByte, 0x11, 0, NULL, 0, ignoreByte},
  {singleByte, 0x12, 0, NULL, 0, ignoreByte},
  {singleByte, 0x13, 0, NULL, 0, ignoreByte},
  {singleByte, 0x14, 0, NULL, 0, ignoreByte},
  {singleByte, 0x15, 0, NULL, 0, ignoreByte},
  {singleByte, 0x16, 0, NULL, 0, ignoreByte},
  {reserved, 0x17, 0, NULL, 0, NULL},
  {reserved, 0x18, 0, NULL, 0, NULL},
  {reserved, 0x19, 0, NULL, 0, NULL},
  {reserved, 0x1A, 0, NULL, 0, NULL},
  {reserved, 0x1B, 0, NULL, 0, NULL},
  {reserved, 0x1C, 0, NULL, 0, NULL},
  {reserved, 0x1D, 0, NULL, 0, NULL},
  {reserved, 0x1E, 0, NULL, 0, NULL},
  {reserved, 0x1F, 0, NULL, 0, NULL},
  {literal, 0x20, 0x20, NULL, 0, NULL},
  {literal, 0x21, 0x21, NULL, 0, NULL},
  {literal, 0x22, 0x22, NULL, 0, NULL},
  {literal, 0x23, 0x23, NULL, 0, NULL},
  {literal, 0x24, 0x24, NULL, 0, NULL},
  {literal, 0x25, 0x25, NULL, 0, NULL},
  {literal, 0x26, 0x26, NULL, 0, NULL},
  {literal, 0x27, 0x27, NULL, 0, NULL},
  {literal, 0x28, 0x28, NULL, 0, NULL},
  {literal, 0x29, 0x29, NULL, 0, NULL},
  {literal, 0x2A, 0x2A, NULL, 0, NULL},
  {literal, 0x2B, 0x2B, NULL, 0, NULL},
  {literal, 0x2C, 0x2C, NULL, 0, NULL},
  {literal, 0x2D, 0x2D, NULL, 0, NULL},
  {literal, 0x2E, 0x2E, NULL, 0, NULL},
  {literal, 0x2F, 0x2F, NULL, 0, NULL},
  {literal, 0x30, 0x30, NULL, 0, NULL},
  {literal, 0x31, 0x31, NULL, 0, NULL},
  {literal, 0x32, 0x32, NULL, 0, NULL},
  {literal, 0x33, 0x33, NULL, 0, NULL},
  {literal, 0x34, 0x34, NULL, 0, NULL},
  {literal, 0x35, 0x35, NULL, 0, NULL},
  {literal, 0x36, 0x36, NULL, 0, NULL},
  {literal, 0x37, 0x37, NULL, 0, NULL},
  {literal, 0x38, 0x38, NULL, 0, NULL},
  {literal, 0x39, 0x39, NULL, 0, NULL},
  {literal, 0x3A, 0x3A, NULL, 0, NULL},
  {literal, 0x3B, 0x3B, NULL, 0, NULL},
  {literal, 0x3C, 0x3C, NULL, 0, NULL},
  {literal, 0x3D, 0x3D, NULL, 0, NULL},
  {literal, 0x3E, 0x3E, NULL, 0, NULL},
  {literal, 0x3F, 0x3F, NULL, 0, NULL},
  {literal, 0x40, 0x40, NULL, 0, NULL},
  {literal, 0x41, 0x41, NULL, 0, NULL},
  {literal, 0x42, 0x42, NULL, 0, NULL},
  {literal, 0x43, 0x43, NULL, 0, NULL},
  {literal, 0x44, 0x44, NULL, 0, NULL},
  {literal, 0x45, 0x45, NULL, 0, NULL},
  {literal, 0x46, 0x46, NULL, 0, NULL},
  {literal, 0x47, 0x47, NULL, 0, NULL},
  {literal, 0x48, 0x48, NULL, 0, NULL},
  {literal, 0x49, 0x49, NULL, 0, NULL},
  {literal, 0x4A, 0x4A, NULL, 0, NULL},
  {literal, 0x4B, 0x4B, NULL, 0, NULL},
  {literal, 0x4C, 0x4C, NULL, 0, NULL},
  {literal, 0x4D, 0x4D, NULL, 0, NULL},
  {literal, 0x4E, 0x4E, NULL, 0, NULL},
  {literal, 0x4F, 0x4F, NULL, 0, NULL},
  {literal, 0x50, 0x50, NULL, 0, NULL},
  {literal, 0x51, 0x51, NULL, 0, NULL},
  {literal, 0x52, 0x52, NULL, 0, NULL},
  {literal, 0x53, 0x53, NULL, 0, NULL},
  {literal, 0x54, 0x54, NULL, 0, NULL},
  {literal, 0x55, 0x55, NULL, 0, NULL},
  {literal, 0x56, 0x56, NULL, 0, NULL},
  {literal, 0x57, 0x57, NULL, 0, NULL},
  {literal, 0x58, 0x58, NULL, 0, NULL},
  {literal, 0x59, 0x59, NULL, 0, NULL},
  {literal, 0x5A, 0x5A, NULL, 0, NULL},
  {literal, 0x5B, 0x5B, NULL, 0, NULL},
  {literal, 0x5C, 0x5C, NULL, 0, NULL},
  {literal, 0x5D, 0x5D, NULL, 0, NULL},
  {literal, 0x5E, 0x5E, NULL, 0, NULL},
  {literal, 0x5F, 0x5F, NULL, 0, NULL},
  {literal, 0x60, 0x60, NULL, 0, NULL},
  {literal, 0x61, 0x61, NULL, 0, NULL},
  {literal, 0x62, 0x62, NULL, 0, NULL},
  {literal, 0x63, 0x63, NULL, 0, NULL},
  {literal, 0x64, 0x64, NULL, 0, NULL},
  {literal, 0x65, 0x65, NULL, 0, NULL},
  {literal, 0x66, 0x66, NULL, 0, NULL},
  {literal, 0x67, 0x67, NULL, 0, NULL},
  {literal, 0x68, 0x68, NULL, 0, NULL},
  {literal, 0x69, 0x69, NULL, 0, NULL},
  {literal, 0x6A, 0x6A, NULL, 0, NULL},
  {literal, 0x6B, 0x6B, NULL, 0, NULL},
  {literal, 0x6C, 0x6C, NULL, 0, NULL},
  {literal, 0x6D, 0x6D, NULL, 0, NULL},
  {literal, 0x6E, 0x6E, NULL, 0, NULL},
  {literal, 0x6F, 0x6F, NULL, 0, NULL},
  {literal, 0x70, 0x70, NULL, 0, NULL},
  {literal, 0x71, 0x71, NULL, 0, NULL},
  {literal, 0x72, 0x72, NULL, 0, NULL},
  {literal, 0x73, 0x73, NULL, 0, NULL},
  {literal, 0x74, 0x74, NULL, 0, NULL},
  {literal, 0x75, 0x75, NULL, 0, NULL},
  {literal, 0x76, 0x76, NULL, 0, NULL},
  {literal, 0x77, 0x77, NULL, 0, NULL},
  {literal, 0x78, 0x78, NULL, 0, NULL},
  {literal, 0x79, 0x79, NULL, 0, NULL},
  {literal, 0x7A, 0x7A, NULL, 0, NULL},
  {literal, 0x7B, 0x7B, NULL, 0, NULL},
  {literal, 0x7C, 0x7C, NULL, 0, NULL},
  {literal, 0x7D, 0x7D, NULL, 0, NULL},
  {literal, 0x7E, 0x7E, NULL, 0, NULL},
  {literal, 0x7F, 0x7F, NULL, 0, NULL},
  {singleByte, 0x80, 0, NULL, 0, ignoreByte},
  {singleByte, 0x81, FlushRight, NULL, 0, outputToken},
  {singleByte, 0x82, eFlushRight, NULL, 0, outputToken},
  {singleByte, 0x83, 0, NULL, 0, popEnvironment},
  {reserved, 0x84, 0, NULL, 0, NULL},
  {singleByte, 0x85, 0, NULL, 0, ignoreByte},
  {singleByte, 0x86, CtrPg, NULL, 0, outputToken},
  {singleByte, 0x87, Col, NULL, 0, outputToken},
  {singleByte, 0x88, eCol, NULL, 0, popEnvironment},
  {reserved, 0x89, 0, NULL, 0, NULL},
  {singleByte, 0x8A, Wid, NULL, 0, outputToken},
  {singleByte, 0x8B, eWid, NULL, 0, outputToken},
  {singleByte, 0x8C, 0, NULL, 0, hardReturnSoftPage},
  {singleByte, 0x8D, FNoteNum, NULL, 0, outputToken},
  {singleByte, 0x8E, FigureNum, NULL, 0, outputToken},
  {singleByte, 0x8F, eCenter, NULL, 0, outputToken},
  {singleByte, 0x90, 0, NULL, 0, ignoreByte},
  {singleByte, 0x91, 0, NULL, 0, ignoreByte},
  {singleByte, 0x92, 0, NULL, 0, ignoreByte},
  {literal, 0x93, 0x20, NULL, 0, outputToken},	/* SRt -> space */
  {singleByte, 0x94, 0, NULL, 0, ignoreByte},
  {singleByte, 0x95, 0, NULL, 0, ignoreByte},
  {singleByte, 0x96, 0, NULL, 0, ignoreByte},	/* block on */
  {singleByte, 0x97, 0, NULL, 0, ignoreByte},	/* block off */
  {singleByte, 0x98, TableMarker, NULL, 0, outputToken},
  {singleByte, 0x99, 0, NULL, 0, hardReturn},
  {singleByte, 0x9A, NoHyphWord, NULL, 0, outputToken},
  {singleByte, 0x9B, 0, NULL, 0, ignoreByte},
  {reserved, 0x9C, 0, NULL, 0, NULL},
  {reserved, 0x9D, Col, NULL, 0, outputToken},
  {singleByte, 0x9E, Hyph, NULL, 0, outputToken},
  {singleByte, 0x9F, eHyph, NULL, 0, outputToken},
  {singleByte, 0xA0, HSpace, NULL, 0, outputToken},
  {singleByte, 0xA1, SubTtl, NULL, 0, outputToken},
  {singleByte, 0xA2, IsSubTtl, NULL, 0, outputToken},
  {singleByte, 0xA3, Ttl, NULL, 0, outputToken},
  {singleByte, 0xA4, IsTtl, NULL, 0, outputToken},
  {singleByte, 0xA5, GrandTtl, NULL, 0, outputToken},
  {singleByte, 0xA6, MathCalcColumn, NULL, 0, outputToken},
  {singleByte, 0xA7, Math, NULL, 0, outputToken},
  {singleByte, 0xA8, eMath, NULL, 0, outputToken},
  {singleByte, 0xA9, HHyph, NULL, 0, outputToken},
  {singleByte, 0xAA, HHyph, NULL, 0, outputToken},
  {singleByte, 0xAB, HHyph, NULL, 0, outputToken},
  {singleByte, 0xAC, NHyph, NULL, 0, outputToken},
  {singleByte, 0xAD, NHyph, NULL, 0, outputToken},
  {singleByte, 0xAE, NHyph, NULL, 0, outputToken},
  {singleByte, 0xAF, eCol, NULL, 0, outputToken},
  {singleByte, 0xB0, eCol, NULL, 0, outputToken},
  {singleByte, 0xB1, NegateTotal, NULL, 0, outputToken},
  {singleByte, 0xB2, eOutline, NULL, 0, outputToken},
  {reserved, 0xB3, 0, NULL, 0, NULL},
  {reserved, 0xB4, 0, NULL, 0, NULL},
  {reserved, 0xB5, 0, NULL, 0, NULL},
  {reserved, 0xB6, 0, NULL, 0, NULL},
  {reserved, 0xB7, 0, NULL, 0, NULL},
  {reserved, 0xB8, 0, NULL, 0, NULL},
  {reserved, 0xB9, 0, NULL, 0, NULL},
  {reserved, 0xBA, 0, NULL, 0, NULL},
  {reserved, 0xBB, 0, NULL, 0, NULL},
  {reserved, 0xBC, 0, NULL, 0, NULL},
  {singleByte, 0xBD, 0, NULL, 0, ignoreByte},
  {singleByte, 0xBE, 0, NULL, 0, ignoreByte},
  {singleByte, 0xBF, 0, NULL, 0, ignoreByte},
  {fixedLength, 0xC0, 0, NULL, 4, extendedChar},
  {fixedLength, 0xC1, 0, NULL, 9, ignoreFixed},		/* Center/Align/Tab/Margin release */
  {fixedLength, 0xC2, 0, NULL, 11, startIndent},	/* Indent */
  {fixedLength, 0xC3, 0, NULL, 3, attributeOn},
  {fixedLength, 0xC4, 0, NULL, 3, attributeOff},
  {fixedLength, 0xC5, 0, NULL, 5, ignoreFixed},		/* Block Protect */
  {fixedLength, 0xC6, 0, NULL, 6, endIndent},	/* End of Indent */
  {fixedLength, 0xC7, 0, NULL, 7, ignoreFixed},		/* Hyphenation stuff */
  {fixedLength, 0xC8, 0, NULL, 4, ignoreFixed},
  {fixedLength, 0xC9, 0, NULL, 5, ignoreFixed},
  {fixedLength, 0xCA, 0, NULL, 6, ignoreFixed},
  {fixedLength, 0xCB, 0, NULL, 6, ignoreFixed},
  {fixedLength, 0xCC, 0, NULL, 8, ignoreFixed},
  {fixedLength, 0xCD, 0, NULL, 10, ignoreFixed},
  {fixedLength, 0xCE, 0, NULL, 10, ignoreFixed},
  {fixedLength, 0xCF, 0, NULL, 12, ignoreFixed},
  {varLength, 0xD0, 0, NULL, 0, ignoreVar},
  {varLength, 0xD1, 0, NULL, 0, ignoreVar},
  {varLength, 0xD2, 0, NULL, 0, ignoreVar},
  {varLength, 0xD3, 0, NULL, 0, ignoreVar},
  {varLength, 0xD4, 0, NULL, 0, ignoreVar},
  {varLength, 0xD5, 0, NULL, 0, ignoreVar},
  {varLength, 0xD6, 0, NULL, 0, ignoreVar},
  {varLength, 0xD7, 0, NULL, 0, ignoreVar},
  {varLength, 0xD8, 0, NULL, 0, ignoreVar},
  {varLength, 0xD9, 0, NULL, 0, ignoreVar},
  {varLength, 0xDA, 0, NULL, 0, ignoreVar},
  {varLength, 0xDB, 0, NULL, 0, ignoreVar},
  {varLength, 0xDC, 0, NULL, 0, ignoreVar},
  {varLength, 0xDD, 0, NULL, 0, ignoreVar},
  {varLength, 0xDE, 0, NULL, 0, ignoreVar},
  {varLength, 0xDF, 0, NULL, 0, ignoreVar},
  {varLength, 0xE0, 0, NULL, 0, ignoreVar},
  {varLength, 0xE1, 0, NULL, 0, ignoreVar},
  {varLength, 0xE2, 0, NULL, 0, ignoreVar},
  {varLength, 0xE3, 0, NULL, 0, ignoreVar},
  {varLength, 0xE4, 0, NULL, 0, ignoreVar},
  {varLength, 0xE5, 0, NULL, 0, ignoreVar},
  {varLength, 0xE6, 0, NULL, 0, ignoreVar},
  {varLength, 0xE7, 0, NULL, 0, ignoreVar},
  {varLength, 0xE8, 0, NULL, 0, ignoreVar},
  {varLength, 0xE9, 0, NULL, 0, ignoreVar},
  {varLength, 0xEA, 0, NULL, 0, ignoreVar},
  {varLength, 0xEB, 0, NULL, 0, ignoreVar},
  {varLength, 0xEC, 0, NULL, 0, ignoreVar},
  {varLength, 0xED, 0, NULL, 0, ignoreVar},
  {varLength, 0xEE, 0, NULL, 0, ignoreVar},
  {varLength, 0xEF, 0, NULL, 0, ignoreVar},
  {varLength, 0xF0, 0, NULL, 0, ignoreVar},
  {varLength, 0xF1, 0, NULL, 0, ignoreVar},
  {varLength, 0xF2, 0, NULL, 0, ignoreVar},
  {varLength, 0xF3, 0, NULL, 0, ignoreVar},
  {varLength, 0xF4, 0, NULL, 0, ignoreVar},
  {varLength, 0xF5, 0, NULL, 0, ignoreVar},
  {varLength, 0xF6, 0, NULL, 0, ignoreVar},
  {varLength, 0xF7, 0, NULL, 0, ignoreVar},
  {varLength, 0xF8, 0, NULL, 0, ignoreVar},
  {varLength, 0xF9, 0, NULL, 0, ignoreVar},
  {varLength, 0xFA, 0, NULL, 0, ignoreVar},
  {varLength, 0xFB, 0, NULL, 0, ignoreVar},
  {varLength, 0xFC, 0, NULL, 0, ignoreVar},
  {varLength, 0xFD, 0, NULL, 0, ignoreVar},
  {varLength, 0xFE, 0, NULL, 0, ignoreVar},
  {varLength, 0xFF, 0, NULL, 0, ignoreVar},
};
