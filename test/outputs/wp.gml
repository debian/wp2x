.* This file was created by the WP2X program, using a GML
.* descriptor file (Version 0.3) written by Raymond Chen.
.*
.* Do not expect everything to be converted perfectly.
.*
.im gmlthesi
.im laser
:GDOC
:FRONTM
:eFRONTM
:BODY
Preparing a document for submissionwith WordPerfect:LQ.Abstract:eLQ.
This document is a demonstration of how papers should be prepared when preparing withWordPerfect. In this document, each section will be formatted in the way that ourconversion software expects to see it. :hp2.Section Headings:ehp2.
Should be numbered, and in a :hp2.VeryLarge:ehp2. font. You may want to make them bold, but that will have no
effect in headers.No blank lines should be left between paragraphs. You may want to turn on display of hard returns bydoing :hp1.:ehp1.:hp1.Shift-F1:ehp1., :hp1.2:ehp1. (Display), :hp1.6:ehp1. (Edit-Screen Options), :hp1.4:ehp1. (Hard Return), :hp1.Control-V:ehp1..
Note, that font changes are perfectly acceptable, so long as you use only :hp2.bold:ehp2., and :hp1.italics:ehp1.. :hp1.Italics:ehp1. will
usually be shown, and many printers actually print :hp1.underlines:ehp1., but some will recall that this is just a
convention that authors used to indicate italics to printers.Our conversion software will not understand other kinds of font changes, and we depend on youindicating headings with specific size changes.:hp2.Spacing of the document:ehp2.
Although we have shown an empty paragraph before and after a heading, there is no real requirementthat you do the same. The heading, once recognized as a heading, will make its own space above andbelow it. It does look a bit odd while composing though.:hp2.Subsections:ehp2.
Are just fine. In order to indicate the depth of the heading, you should use a smaller font.In other words, use :hp2.ExtraLarge:ehp2. for titles, :hp2.VeryLarge:ehp2. for major headings, :hp2.Large:ehp2. for the first level of
subheadings, onwards through to :hp2.Small:ehp2. and :hp2.Fine:ehp2.. This gives you four levels of headings, and a title font,
which should be more than sufficient. Number your headings if you like.:hp2.Bibliography:ehp2.
The bibliography should be a heading on its own, with the word :hp1.Bibliography:ehp1. as its heading. It should
be formatted just like you would format any bibliography. Here is our suggest mechanism for formatting::hp2.:LQ.[Walthers80]:ehp2.:hp1.Feline disposal methods for radioactive waste:ehp1., Moron, J. et al, Cat's Quarterly, V. 3
winter, pp. 14-57.:eLQ.
.*
:eBODY
:eGDOC
.* End of file
